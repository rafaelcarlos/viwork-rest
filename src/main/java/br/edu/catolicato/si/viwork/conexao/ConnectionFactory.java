package br.edu.catolicato.si.viwork.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {

    private static Connection connection;

    private static final String HOST = "localhost";
    private static final String BD = "viwork";
    private static final String USER = "root";
    private static final String PWD = "root";

    private ConnectionFactory() {}

    public static Connection getConnectionSQL() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (connection == null) {
            Properties p = new Properties();
            p.put("user", USER);
            p.put("password", PWD);
            p.put("rewriteBatchedStatement", "true");
            connection = DriverManager.getConnection("jdbc:mysql://" + HOST + ":3306/" + BD + "?autoReconnect=true&useUnicode=true&characterEncoding=utf8", p);
        }
        return connection;
    }
}
