package br.edu.catolicato.si.viwork.servico;

import br.edu.catolicato.si.viwork.modelo.Postagem;
import br.edu.catolicato.si.viwork.modelo.PostagemDAO;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Luídne
 */
@Path("postagem")
public class PostagemREST {
    
    private PostagemDAO dao;
    
    public PostagemREST() {
        this.dao = new PostagemDAO();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void inserir(Postagem postagem) {
        this.dao.inserir(postagem);
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String atualizar(@PathParam("id") Integer id, Postagem postagem) {
        return this.dao.atualizar(postagem) ? "true" : "false";
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") Integer id) {
        this.dao.excluir(id);
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Postagem buscar(@PathParam("id") Integer id) {
        return dao.buscarPorId(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Postagem> buscar() {
//        return this.dao.buscarTudo();
        List<Postagem> postagens = Arrays
                .asList(new Postagem(1, "Texto 1"), new Postagem(2, "Texto 2"));
        
        return postagens;
    }
    
}
