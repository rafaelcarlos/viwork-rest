package br.edu.catolicato.si.viwork;

import javax.ws.rs.core.Application;

/**
 *
 * @author Luídne
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {    
}
