package br.edu.catolicato.si.viwork.modelo;

import br.edu.catolicato.si.viwork.conexao.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luídne
 */
public class PostagemDAO {

    private Conexao conexao;

    public PostagemDAO() {
        this.conexao = new Conexao();
    }

    public Postagem buscarPorId(int id) {
        String query = "SELECT * FROM " + Postagem.TABELA + " WHERE " + Postagem.ID + "=?";

        conexao.preparar(query);
        try {
            conexao.getPstmt().setInt(1, id);
            ResultSet rs = conexao.executeQuery();
            while (rs.next()) {
                return this.preencherModelo(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostagemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    public List<Postagem> buscarTudo() {
        String query = "SELECT * FROM " + Postagem.TABELA + " ORDER BY " + Postagem.DATA + " DESC";
        List<Postagem> lista = new ArrayList<>();

        conexao.preparar(query);
        try {
            ResultSet rs = conexao.executeQuery();
            while (rs.next()) {
                lista.add(this.preencherModelo(rs));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return lista;
        }
    }

    public int inserir(Postagem obj) {
        String query = "INSERT INTO " + Postagem.TABELA + " (" + Postagem.TEXTO + "," + Postagem.DATA + ") VALUES (?,?)";

        conexao.preparar(query);
        try {
            conexao.getPstmt().setString(1, obj.getTexto());
            conexao.getPstmt().setTimestamp(2, new Timestamp(obj.getData().getTime()));

            conexao.executeUpdate();
            ResultSet rs = conexao.getPrimaryKey();
            if (rs.next()) {
                obj.setId(rs.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return obj.getId();
        }
    }
    
    public boolean atualizar(Postagem postagem)
    {
        String query = "UPDATE " + Postagem.TABELA + " SET " + Postagem.TEXTO + "=?" + " WHERE " + Postagem.ID + "=?";
        this.conexao.preparar(query);
        try
        {
            this.conexao.getPstmt().setString(1, postagem.getTexto());
            this.conexao.getPstmt().setInt(2, postagem.getId());
            return conexao.executeUpdate();
        } catch (SQLException ex)
        {
            Logger.getLogger(PostagemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean excluir(int id)
    {
        String query = "DELETE FROM " + Postagem.TABELA + " WHERE " + Postagem.ID + "=?";
        this.conexao.preparar(query);
        try
        {
            this.conexao.getPstmt().setInt(1, id);
            return conexao.executeUpdate();
        } catch (SQLException ex)
        {
            Logger.getLogger(PostagemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private Postagem preencherModelo(ResultSet rs) {
        Postagem postagem = new Postagem();
        try {
            postagem.setId(rs.getInt(Postagem.ID));
            postagem.setTexto(rs.getString(Postagem.TEXTO));
            postagem.setData(rs.getTimestamp(Postagem.DATA));

            return postagem;
        } catch (SQLException ex) {
            Logger.getLogger(PostagemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
