package br.edu.catolicato.si.viwork.conexao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao implements Serializable {

    private Connection connection;
    private PreparedStatement pstmt;

    public Conexao() {
        try {
            this.connection = ConnectionFactory.getConnectionSQL();
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void fechar() {
        try {
            this.connection.close();
            this.pstmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void preparar(String query) {
        try {
            if (this.connection.isClosed()) {
                this.connection = ConnectionFactory.getConnectionSQL();
            }
            this.pstmt = this.connection.prepareStatement(query);
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet executeQuery() throws SQLException {
        return (this.pstmt.executeQuery());
    }

    public boolean executeUpdate() throws SQLException {
        return (this.pstmt.executeUpdate() > 0);
    }

    public PreparedStatement getPstmt() {
        return pstmt;
    }

    public ResultSet getPrimaryKey() {
        try {
            return pstmt.getGeneratedKeys();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
